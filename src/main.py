from aiohttp import web, WSMsgType

import asyncio
import uvloop

import binascii
import json
import rsa
import os
import random

import logging
logging.basicConfig(level=logging.INFO)

clients = set()

async def _websocket(request):
    websocket = web.WebSocketResponse()
    await websocket.prepare(request)

    logging.info(f'New connection {websocket}')

    clients.add(websocket)
    
    bob = Bob(websocket)
    alice = Alice(websocket)
    
    alice.bob = bob
    bob.alice = alice
    
    alice.bots = [alice, bob]
    bob.bots = [alice, bob]
    
    task = asyncio.ensure_future(alice.worker())
    
    
    try:
        async for message in websocket:
            if message.type == WSMsgType.TEXT:
                data = message.data
                asyncio.ensure_future(bob.listen(data))
                asyncio.ensure_future(alice.listen(data))
                _, recipient, __, *___ = json.loads(data)
                if recipient == bob.id or recipient == alice.id:
                    continue
                for client in clients - {websocket}:
                    try: await client.send_str(data)
                    except: clients.remove(client)
    except Exception as e:
        logging.error(e)
    if websocket in clients:
         clients.remove(websocket)
    task.cancel()
    
    return websocket

KEY_SIZE = 512
   
def encrypt(data, key):
    res = b''
    data = data.encode()
    for i in range(0, len(data), 53):
        res += rsa.encrypt(data[i:i+53], key)
    return binascii.hexlify(res).decode()

def decrypt(data, key):
    res = b''
    data = binascii.unhexlify(data)
    for i in range(0, len(data), 64):
        res += rsa.decrypt(data[i:i+64], key)
    return res.decode()

class Bot:
    def __init__(self, websocket):
        self.keys = {}
        self._websocket = websocket
        self.id = binascii.hexlify(os.urandom(6)).decode()

    async def listen(self, message):
        await asyncio.sleep(1)
        try:
            sender, recipient, cmd, *args = json.loads(message)
            if recipient != self.id and recipient != '@all':
                return
            if hasattr(self, cmd):
                fn = getattr(self, cmd)
                if asyncio.iscoroutinefunction(fn):
                    await fn(sender, recipient, *args)
                else:
                    fn(sender, recipient, *args)
        except Exception as e:
            logging.exception(e)

    async def start_handshake(self, from_, to, public):
        if to == '@all' or from_ in self.keys: return
        public = rsa.PublicKey.load_pkcs1(public)
        publicKey, private = rsa.newkeys(KEY_SIZE)
        self.keys[from_] = {'public': public, 'private': private}
        self.send('end_handshake', publicKey.save_pkcs1().decode(), to=from_)

    async def end_handshake(self, from_, to, public):
        if to == '@all' or from_ not in self.keys: return
        self.keys[from_]['public'] = rsa.PublicKey.load_pkcs1(public)

    def send(self, *args, to='@all'):
        data = json.dumps([self.id, to, *args])
        if to == self.bots[0].id:
            asyncio.ensure_future(self.bots[0].listen(data))
        elif to == self.bots[1].id:
            asyncio.ensure_future(self.bots[1].listen(data))
        asyncio.ensure_future(self._websocket.send_str(data))

    def send_message(self, message, to):
        self.send('message', encrypt(message, self.keys[to]['public']), to=to)

class Alice(Bot):
    async def worker(self):
        await asyncio.sleep(2)
        while True:
            try:
                self.id = binascii.hexlify(os.urandom(6)).decode()
                self.bob.id = binascii.hexlify(os.urandom(6)).decode()
                
                connected_queue = [self, self.bob]
                connected_queue.pop(random.random() > .5).send('connected')
                await asyncio.sleep(random.random() * 3)
                connected_queue[0].send('connected')

                self.correct_password = False

                self.bob.password = binascii.hexlify(os.urandom(3)).decode()
                self.bob.keyphrase = binascii.hexlify(os.urandom(3)).decode()
                self.bob.public = self.bob.private = self.public = self.private = None

                publicKey, private = rsa.newkeys(KEY_SIZE)
                self.keys[self.bob.id] = {'private': private}

                await asyncio.sleep(5 + random.random() * 5)
                while 'public' not in self.keys[self.bob.id]:
                    self.send('start_handshake', publicKey.save_pkcs1().decode(), to=self.bob.id)
                    await asyncio.sleep(10)

                self.send_message('excellent, finally I was able to contact you, '+self.bob.keyphrase+', now I will give you secret data, but first tell me the password so that I can make sure that you are the one we need', self.bob.id)

                await asyncio.sleep(10)

                if not self.correct_password:
                    self.send_message('you suspiciously hesitate to answer, I do not trust you, goodbye', self.bob.id)
                else:
                    while self.transmission:
                        await asyncio.sleep(1)

                del self.keys[self.bob.id]
                del self.bob.keys[self.id]
            except Exception as e:
                logging.exception(e)
            finally:
                await asyncio.sleep(30)

    async def message(self, from_, to, text):
        if to == '@all': return
        try:
            text = decrypt(text, self.keys[from_]['private'])
        except:
            return
        if from_ != self.bob.id:
            logging.info(f'{from_}: {text}')
            self.send_message("I'm sorry, I'm not open to relationships with strangers.", from_)
            self.send_message("don't write here anymore", from_)
            del self.keys[from_]
        else:
            if not self.correct_password:
                if text != self.bob.password:
                    self.send_message("the connection was compromised, you need to hurry", from_)
                self.correct_password = True
                self.transmission = True
                await asyncio.sleep(3)
                self.send_message('good, now listen closely. our future victory depends on it', from_)
                await asyncio.sleep(3)
                self.send_message('this is the data that will save everyone', from_)
                await asyncio.sleep(2)
                self.send_message(os.getenv('FLAG'), from_)
                await asyncio.sleep(1)
                self.send_message('pass on to your comrades', from_)
                await asyncio.sleep(2)
                self.send_message('we are preparing a retaliatory strike', from_)
                await asyncio.sleep(0.2)
                self.send_message('bye', from_)
                self.transmission = False

class Bob(Bot):
    def message(self, from_, to, text):
        if to == '@all': return
        try:
            text = decrypt(text, self.keys[from_]['private'])
        except:
            return
        if from_ != self.alice.id:
            logging.info(f'{from_}: {text}')
            self.send_message("you don't look like the one I have to contact, goodbye", from_)
            del self.keys[from_]
        elif self.keyphrase in text:
            self.send_message(self.password, from_)

async def on_shutdown(app):
    for websocket in _clients:
        await ws.close(code=1001, message='Server shutdown')

loop = uvloop.new_event_loop()
asyncio.set_event_loop(loop)

app = web.Application()
app.router.add_route('GET', '/api', _websocket, name='websocket')
app.on_shutdown.append(on_shutdown)

web.run_app(app, host='0.0.0.0', port=8765)
