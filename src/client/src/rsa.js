function chars_from_hex(d) {
    var e = "";
    d = d.replace(/^(0x)?/g, ""),
    d = d.replace(/[^A-Fa-f0-9]/g, ""),
    d = d.split("");
    for (var f = 0; f < d.length; f += 2)
        e += String.fromCharCode(parseInt(d[f] + "" + d[f + 1], 16));
    return e
}
function encode64(j) {
    j = j.replace(/\0*$/g, "");
    var k, l, m, n, o = "", p = "", q = "", r = 0;
    do
        k = j.charCodeAt(r++),
        l = j.charCodeAt(r++),
        p = j.charCodeAt(r++),
        m = k >> 2,
        k = (3 & k) << 4 | l >> 4,
        n = (15 & l) << 2 | p >> 6,
        q = 63 & p,
        isNaN(l) ? n = q = 64 : isNaN(p) && (q = 64),
        o = o + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(m) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(k) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(n) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(q);
    while (r < j.length);for (j = "",
    o = o.split(""),
    r = 0; r < o.length; r++)
        0 == r % 64 && 0 < r && (j += "\n"),
        j += o[r];
    for (o.join(),
    o = j % 4,
    r = 0; r < o; r++)
        j += "=";
    return j
}
function decode64(c) {
    var j, k, l, m = "", n = "", o = "", p = 0;
    c = c.replace(/[^A-Za-z0-9\+\/\=\/n]/g, "");
    do
        j = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(c.charAt(p++)),
        k = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(c.charAt(p++)),
        l = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(c.charAt(p++)),
        o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(c.charAt(p++)),
        j = j << 2 | k >> 4,
        k = (15 & k) << 4 | l >> 2,
        n = (3 & l) << 6 | o,
        m += String.fromCharCode(j),
        64 != l && (m += String.fromCharCode(k)),
        64 != o && (m += String.fromCharCode(n));
    while (p < c.length);return m = m.replace(/\0*$/g, "")
}
var dbits, canary = 244837814094590, j_lm = !0;
function BigInteger(d, a, b) {
    null != d && ("number" == typeof d ? this.fromNumber(d, a, b) : null == a && "string" != typeof d ? this.fromString(d, 256) : this.fromString(d, a))
}
function nbi() {
    return new BigInteger(null)
}
function am1(h, i, b, c, j, k) {
    for (; 0 <= --k; ) {
        var l = i * this[h++] + b[c] + j;
        j = Math.floor(l / 67108864),
        b[c++] = 67108863 & l
    }
    return j
}
function am2(j, l, m, c, n, o) {
    var p = 32767 & l;
    for (l >>= 15; 0 <= --o; ) {
        var g = 32767 & this[j]
          , q = this[j++] >> 15
          , i = l * g + q * p;
        g = p * g + ((32767 & i) << 15) + m[c] + (1073741823 & n),
        n = (g >>> 30) + (i >>> 15) + l * q + (n >>> 30),
        m[c++] = 1073741823 & g
    }
    return n
}
function am3(j, l, m, c, n, o) {
    var p = 16383 & l;
    for (l >>= 14; 0 <= --o; ) {
        var g = 16383 & this[j]
          , q = this[j++] >> 14
          , i = l * g + q * p;
        g = p * g + ((16383 & i) << 14) + m[c] + n,
        n = (g >> 28) + (i >> 14) + l * q,
        m[c++] = 268435455 & g
    }
    return n
}
"Microsoft Internet Explorer" == navigator.appName ? (BigInteger.prototype.am = am2,
dbits = 30) : "Netscape" != navigator.appName ? (BigInteger.prototype.am = am1,
dbits = 26) : (BigInteger.prototype.am = am3,
dbits = 28),
BigInteger.prototype.DB = dbits,
BigInteger.prototype.DM = (1 << dbits) - 1,
BigInteger.prototype.DV = 1 << dbits;
var BI_FP = 52;
BigInteger.prototype.FV = Math.pow(2, 52),
BigInteger.prototype.F1 = 52 - dbits,
BigInteger.prototype.F2 = 2 * dbits - 52;
var rr, vv, BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz", BI_RC = [];
for (rr = 48,
vv = 0; 9 >= vv; ++vv)
    BI_RC[rr++] = vv;
for (rr = 97,
vv = 10; 36 > vv; ++vv)
    BI_RC[rr++] = vv;
for (rr = 65,
vv = 10; 36 > vv; ++vv)
    BI_RC[rr++] = vv;
function int2char(b) {
    return BI_RM.charAt(b)
}
function intAt(d, a) {
    var b = BI_RC[d.charCodeAt(a)];
    return null == b ? -1 : b
}
function bnpCopyTo(c) {
    for (var a = this.t - 1; 0 <= a; --a)
        c[a] = this[a];
    c.t = this.t,
    c.s = this.s
}
function bnpFromInt(b) {
    this.t = 1,
    this.s = 0 > b ? -1 : 0,
    0 < b ? this[0] = b : -1 > b ? this[0] = b + DV : this.t = 0
}
function nbv(c) {
    var a = nbi();
    return a.fromInt(c),
    a
}
function bnpFromString(h, a) {
    var b;
    if (16 == a)
        b = 4;
    else if (8 == a)
        b = 3;
    else if (256 == a)
        b = 8;
    else if (2 == a)
        b = 1;
    else if (32 == a)
        b = 5;
    else if (4 == a)
        b = 2;
    else
        return void this.fromRadix(h, a);
    this.s = this.t = 0;
    for (var i, j = h.length, k = !1, l = 0; 0 <= --j; )
        i = 8 == b ? 255 & h[j] : intAt(h, j),
        0 > i ? "-" == h.charAt(j) && (k = !0) : (k = !1,
        0 == l ? this[this.t++] = i : l + b > this.DB ? (this[this.t - 1] |= (i & (1 << this.DB - l) - 1) << l,
        this[this.t++] = i >> this.DB - l) : this[this.t - 1] |= i << l,
        l += b,
        l >= this.DB && (l -= this.DB));
    8 == b && 0 != (128 & h[0]) && (this.s = -1,
    0 < l && (this[this.t - 1] |= (1 << this.DB - l) - 1 << l)),
    this.clamp(),
    k && BigInteger.ZERO.subTo(this, this)
}
function bnpClamp() {
    for (var b = this.s & this.DM; 0 < this.t && this[this.t - 1] == b; )
        --this.t
}
function bnToString(h) {
    if (0 > this.s)
        return "-" + this.negate().toString(h);
    if (16 == h)
        h = 4;
    else if (8 == h)
        h = 3;
    else if (2 == h)
        h = 1;
    else if (32 == h)
        h = 5;
    else if (4 == h)
        h = 2;
    else
        return this.toRadix(h);
    var i, j = (1 << h) - 1, b = !1, k = "", l = this.t, m = this.DB - l * this.DB % h;
    if (0 < l--)
        for (m < this.DB && 0 < (i = this[l] >> m) && (b = !0,
        k = int2char(i)); 0 <= l; )
            m < h ? (i = (this[l] & (1 << m) - 1) << h - m,
            i |= this[--l] >> (m += this.DB - h)) : (i = this[l] >> (m -= h) & j,
            0 >= m && (m += this.DB,
            --l)),
            0 < i && (b = !0),
            b && (k += int2char(i));
    return b ? k : "0"
}
function bnNegate() {
    var b = nbi();
    return BigInteger.ZERO.subTo(this, b),
    b
}
function bnAbs() {
    return 0 > this.s ? this.negate() : this
}
function bnCompareTo(d) {
    var a = this.s - d.s;
    if (0 != a)
        return a;
    var e = this.t;
    if (a = e - d.t,
    0 != a)
        return a;
    for (; 0 <= --e; )
        if (0 != (a = this[e] - d[e]))
            return a;
    return 0
}
function nbits(d) {
    var e, f = 1;
    return 0 != (e = d >>> 16) && (d = e,
    f += 16),
    0 != (e = d >> 8) && (d = e,
    f += 8),
    0 != (e = d >> 4) && (d = e,
    f += 4),
    0 != (e = d >> 2) && (d = e,
    f += 2),
    0 != d >> 1 && (f += 1),
    f
}
function bnBitLength() {
    return 0 >= this.t ? 0 : this.DB * (this.t - 1) + nbits(this[this.t - 1] ^ this.s & this.DM)
}
function bnpDLShiftTo(d, a) {
    var b;
    for (b = this.t - 1; 0 <= b; --b)
        a[b + d] = this[b];
    for (b = d - 1; 0 <= b; --b)
        a[b] = 0;
    a.t = this.t + d,
    a.s = this.s
}
function bnpDRShiftTo(d, a) {
    for (var b = d; b < this.t; ++b)
        a[b - d] = this[b];
    a.t = Math.max(this.t - d, 0),
    a.s = this.s
}
function bnpLShiftTo(f, a) {
    var b, i = f % this.DB, c = this.DB - i, d = Math.floor(f / this.DB), e = this.s << i & this.DM;
    for (b = this.t - 1; 0 <= b; --b)
        a[b + d + 1] = this[b] >> c | e,
        e = (this[b] & (1 << c) - 1) << i;
    for (b = d - 1; 0 <= b; --b)
        a[b] = 0;
    a[d] = e,
    a.t = this.t + d + 1,
    a.s = this.s,
    a.clamp()
}
function bnpRShiftTo(h, a) {
    a.s = this.s;
    var b = Math.floor(h / this.DB);
    if (b >= this.t)
        a.t = 0;
    else {
        var c = h % this.DB
          , d = this.DB - c
          , f = (1 << c) - 1;
        a[0] = this[b] >> c;
        for (var e = b + 1; e < this.t; ++e)
            a[e - b - 1] |= (this[e] & f) << d,
            a[e - b] = this[e] >> c;
        0 < c && (a[this.t - b - 1] |= (this.s & f) << d),
        a.t = this.t - b,
        a.clamp()
    }
}
function bnpSubTo(e, a) {
    for (var b = 0, g = 0, h = Math.min(e.t, this.t); b < h; )
        g += this[b] - e[b],
        a[b++] = g & this.DM,
        g >>= this.DB;
    if (e.t < this.t) {
        for (g -= e.s; b < this.t; )
            g += this[b],
            a[b++] = g & this.DM,
            g >>= this.DB;
        g += this.s
    } else {
        for (g += this.s; b < e.t; )
            g -= e[b],
            a[b++] = g & this.DM,
            g >>= this.DB;
        g -= e.s
    }
    a.s = 0 > g ? -1 : 0,
    -1 > g ? a[b++] = this.DV + g : 0 < g && (a[b++] = g),
    a.t = b,
    a.clamp()
}
function bnpMultiplyTo(e, a) {
    var b = this.abs()
      , c = e.abs()
      , d = b.t;
    for (a.t = d + c.t; 0 <= --d; )
        a[d] = 0;
    for (d = 0; d < c.t; ++d)
        a[d + b.t] = b.am(0, c[d], a, d, 0, b.t);
    a.s = 0,
    a.clamp(),
    this.s != e.s && BigInteger.ZERO.subTo(a, a)
}
function bnpSquareTo(e) {
    for (var a = this.abs(), b = e.t = 2 * a.t; 0 <= --b; )
        e[b] = 0;
    for (b = 0; b < a.t - 1; ++b) {
        var f = a.am(b, a[b], e, 2 * b, 0, 1);
        (e[b + a.t] += a.am(b + 1, 2 * a[b], e, 2 * b + 1, f, a.t - b - 1)) >= a.DV && (e[b + a.t] -= a.DV,
        e[b + a.t + 1] = 1)
    }
    0 < e.t && (e[e.t - 1] += a.am(b, a[b], e, 2 * b, 0, 1)),
    e.s = 0,
    e.clamp()
}
function bnpDivRemTo(p, q, b) {
    var r = p.abs();
    if (!(0 >= r.t)) {
        var s = this.abs();
        if (s.t < r.t)
            null != q && q.fromInt(0),
            null != b && this.copyTo(b);
        else {
            null == b && (b = nbi());
            var t = nbi()
              , e = this.s;
            p = p.s;
            var g = this.DB - nbits(r[r.t - 1]);
            if (0 < g ? (r.lShiftTo(g, t),
            s.lShiftTo(g, b)) : (r.copyTo(t),
            s.copyTo(b)),
            r = t.t,
            s = t[r - 1],
            0 != s) {
                var h = s * (1 << this.F1) + (1 < r ? t[r - 2] >> this.F2 : 0)
                  , u = this.FV / h;
                h = (1 << this.F1) / h;
                var k = 1 << this.F2
                  , o = b.t
                  , v = o - r
                  , w = null == q ? nbi() : q;
                for (t.dlShiftTo(v, w),
                0 <= b.compareTo(w) && (b[b.t++] = 1,
                b.subTo(w, b)),
                BigInteger.ONE.dlShiftTo(r, w),
                w.subTo(t, t); t.t < r; )
                    t[t.t++] = 0;
                for (; 0 <= --v; ) {
                    var j = b[--o] == s ? this.DM : Math.floor(b[o] * u + (b[o - 1] + k) * h);
                    if ((b[o] += t.am(0, j, b, v, 0, r)) < j)
                        for (t.dlShiftTo(v, w),
                        b.subTo(w, b); b[o] < --j; )
                            b.subTo(w, b)
                }
                null != q && (b.drShiftTo(r, q),
                e != p && BigInteger.ZERO.subTo(q, q)),
                b.t = r,
                b.clamp(),
                0 < g && b.rShiftTo(g, b),
                0 > e && BigInteger.ZERO.subTo(b, b)
            }
        }
    }
}
function bnMod(c) {
    var a = nbi();
    return this.abs().divRemTo(c, null, a),
    0 > this.s && 0 < a.compareTo(BigInteger.ZERO) && c.subTo(a, a),
    a
}
function Classic(b) {
    this.m = b
}
function cConvert(b) {
    return 0 > b.s || 0 <= b.compareTo(this.m) ? b.mod(this.m) : b
}
function cRevert(b) {
    return b
}
function cReduce(b) {
    b.divRemTo(this.m, null, b)
}
function cMulTo(d, a, b) {
    d.multiplyTo(a, b),
    this.reduce(b)
}
function cSqrTo(c, a) {
    c.squareTo(a),
    this.reduce(a)
}
Classic.prototype.convert = cConvert,
Classic.prototype.revert = cRevert,
Classic.prototype.reduce = cReduce,
Classic.prototype.mulTo = cMulTo,
Classic.prototype.sqrTo = cSqrTo;
function bnpInvDigit() {
    if (1 > this.t)
        return 0;
    var c = this[0];
    if (0 == (1 & c))
        return 0;
    var a = 3 & c;
    return a = 15 & a * (2 - (15 & c) * a),
    a = 255 & a * (2 - (255 & c) * a),
    a = 65535 & a * (2 - (65535 & (65535 & c) * a)),
    a = a * (2 - c * a % this.DV) % this.DV,
    0 < a ? this.DV - a : -a
}
function Montgomery(b) {
    this.m = b,
    this.mp = b.invDigit(),
    this.mpl = 32767 & this.mp,
    this.mph = this.mp >> 15,
    this.um = (1 << b.DB - 15) - 1,
    this.mt2 = 2 * b.t
}
function montConvert(c) {
    var a = nbi();
    return c.abs().dlShiftTo(this.m.t, a),
    a.divRemTo(this.m, null, a),
    0 > c.s && 0 < a.compareTo(BigInteger.ZERO) && this.m.subTo(a, a),
    a
}
function montRevert(c) {
    var a = nbi();
    return c.copyTo(a),
    this.reduce(a),
    a
}
function montReduce(e) {
    for (; e.t <= this.mt2; )
        e[e.t++] = 0;
    for (var a = 0; a < this.m.t; ++a) {
        var f = 32767 & e[a]
          , g = f * this.mpl + ((f * this.mph + (e[a] >> 15) * this.mpl & this.um) << 15) & e.DM;
        for (f = a + this.m.t,
        e[f] += this.m.am(0, g, e, a, 0, this.m.t); e[f] >= e.DV; )
            e[f] -= e.DV,
            e[++f]++
    }
    e.clamp(),
    e.drShiftTo(this.m.t, e),
    0 <= e.compareTo(this.m) && e.subTo(this.m, e)
}
function montSqrTo(c, a) {
    c.squareTo(a),
    this.reduce(a)
}
function montMulTo(d, a, b) {
    d.multiplyTo(a, b),
    this.reduce(b)
}
Montgomery.prototype.convert = montConvert,
Montgomery.prototype.revert = montRevert,
Montgomery.prototype.reduce = montReduce,
Montgomery.prototype.mulTo = montMulTo,
Montgomery.prototype.sqrTo = montSqrTo;
function bnpIsEven() {
    return 0 == (0 < this.t ? 1 & this[0] : this.s)
}
function bnpExp(h, a) {
    if (4294967295 < h || 1 > h)
        return BigInteger.ONE;
    var b = nbi()
      , i = nbi()
      , j = a.convert(this)
      , f = nbits(h) - 1;
    for (j.copyTo(b); 0 <= --f; )
        if (a.sqrTo(b, i),
        0 < (h & 1 << f))
            a.mulTo(i, j, b);
        else {
            var k = b;
            b = i,
            i = k
        }
    return a.revert(b)
}
function bnModPowInt(d, a) {
    var b;
    return b = 256 > d || a.isEven() ? new Classic(a) : new Montgomery(a),
    this.exp(d, b)
}
BigInteger.prototype.copyTo = bnpCopyTo,
BigInteger.prototype.fromInt = bnpFromInt,
BigInteger.prototype.fromString = bnpFromString,
BigInteger.prototype.clamp = bnpClamp,
BigInteger.prototype.dlShiftTo = bnpDLShiftTo,
BigInteger.prototype.drShiftTo = bnpDRShiftTo,
BigInteger.prototype.lShiftTo = bnpLShiftTo,
BigInteger.prototype.rShiftTo = bnpRShiftTo,
BigInteger.prototype.subTo = bnpSubTo,
BigInteger.prototype.multiplyTo = bnpMultiplyTo,
BigInteger.prototype.squareTo = bnpSquareTo,
BigInteger.prototype.divRemTo = bnpDivRemTo,
BigInteger.prototype.invDigit = bnpInvDigit,
BigInteger.prototype.isEven = bnpIsEven,
BigInteger.prototype.exp = bnpExp,
BigInteger.prototype.toString = bnToString,
BigInteger.prototype.negate = bnNegate,
BigInteger.prototype.abs = bnAbs,
BigInteger.prototype.compareTo = bnCompareTo,
BigInteger.prototype.bitLength = bnBitLength,
BigInteger.prototype.mod = bnMod,
BigInteger.prototype.modPowInt = bnModPowInt,
BigInteger.ZERO = nbv(0),
BigInteger.ONE = nbv(1);
function bnClone() {
    var b = nbi();
    return this.copyTo(b),
    b
}
function bnIntValue() {
    if (0 > this.s) {
        if (1 == this.t)
            return this[0] - this.DV;
        if (0 == this.t)
            return -1
    } else {
        if (1 == this.t)
            return this[0];
        if (0 == this.t)
            return 0
    }
    return (this[1] & (1 << 32 - this.DB) - 1) << this.DB | this[0]
}
function bnByteValue() {
    return 0 == this.t ? this.s : this[0] << 24 >> 24
}
function bnShortValue() {
    return 0 == this.t ? this.s : this[0] << 16 >> 16
}
function bnpChunkSize(b) {
    return Math.floor(Math.LN2 * this.DB / Math.log(b))
}
function bnSigNum() {
    return 0 > this.s ? -1 : 0 >= this.t || 1 == this.t && 0 >= this[0] ? 0 : 1
}
function bnpToRadix(f) {
    if (null == f && (f = 10),
    0 == this.signum() || 2 > f || 36 < f)
        return "0";
    var h = this.chunkSize(f);
    h = Math.pow(f, h);
    var i = nbv(h)
      , c = nbi()
      , d = nbi()
      , e = "";
    for (this.divRemTo(i, c, d); 0 < c.signum(); )
        e = (h + d.intValue()).toString(f).substr(1) + e,
        c.divRemTo(i, c, d);
    return d.intValue().toString(f) + e
}
function bnpFromRadix(i, a) {
    this.fromInt(0),
    null == a && (a = 10);
    for (var j, l = this.chunkSize(a), c = Math.pow(a, l), d = !1, m = 0, n = 0, o = 0; o < i.length; ++o)
        j = intAt(i, o),
        0 > j ? "-" == i.charAt(o) && 0 == this.signum() && (d = !0) : (n = a * n + j,
        ++m >= l && (this.dMultiply(c),
        this.dAddOffset(n, 0),
        n = m = 0));
    0 < m && (this.dMultiply(Math.pow(a, m)),
    this.dAddOffset(n, 0)),
    d && BigInteger.ZERO.subTo(this, this)
}
function bnpFromNumber(e, a, b) {
    if (!("number" == typeof a)) {
        b = [];
        var f = 7 & e;
        b.length = (e >> 3) + 1,
        a.nextBytes(b),
        0 < f ? b[0] &= (1 << f) - 1 : b[0] = 0,
        this.fromString(b, 256)
    } else if (2 > e)
        this.fromInt(1);
    else
        for (this.fromNumber(e, b),
        this.testBit(e - 1) || this.bitwiseTo(BigInteger.ONE.shiftLeft(e - 1), op_or, this),
        this.isEven() && this.dAddOffset(1, 0); !this.isProbablePrime(a); )
            this.dAddOffset(2, 0),
            this.bitLength() > e && this.subTo(BigInteger.ONE.shiftLeft(e - 1), this)
}
function bnToByteArray() {
    var f = this.t
      , g = [];
    g[0] = this.s;
    var b, h = this.DB - f * this.DB % 8, i = 0;
    if (0 < f--)
        for (h < this.DB && (b = this[f] >> h) != (this.s & this.DM) >> h && (g[i++] = b | this.s << this.DB - h); 0 <= f; )
            8 > h ? (b = (this[f] & (1 << h) - 1) << 8 - h,
            b |= this[--f] >> (h += this.DB - 8)) : (b = 255 & this[f] >> (h -= 8),
            0 >= h && (h += this.DB,
            --f)),
            0 != (128 & b) && (b |= -256),
            0 == i && (128 & this.s) != (128 & b) && ++i,
            (0 < i || b != this.s) && (g[i++] = b);
    return g
}
function bnEquals(b) {
    return 0 == this.compareTo(b)
}
function bnMin(b) {
    return 0 > this.compareTo(b) ? this : b
}
function bnMax(b) {
    return 0 < this.compareTo(b) ? this : b
}
function bnpBitwiseTo(f, a, b) {
    var c, h, i = Math.min(f.t, this.t);
    for (c = 0; c < i; ++c)
        b[c] = a(this[c], f[c]);
    if (f.t < this.t) {
        for (h = f.s & this.DM,
        c = i; c < this.t; ++c)
            b[c] = a(this[c], h);
        b.t = this.t
    } else {
        for (h = this.s & this.DM,
        c = i; c < f.t; ++c)
            b[c] = a(h, f[c]);
        b.t = f.t
    }
    b.s = a(this.s, f.s),
    b.clamp()
}
function op_and(c, a) {
    return c & a
}
function bnAnd(c) {
    var a = nbi();
    return this.bitwiseTo(c, op_and, a),
    a
}
function op_or(c, a) {
    return c | a
}
function bnOr(c) {
    var a = nbi();
    return this.bitwiseTo(c, op_or, a),
    a
}
function op_xor(c, a) {
    return c ^ a
}
function bnXor(c) {
    var a = nbi();
    return this.bitwiseTo(c, op_xor, a),
    a
}
function op_andnot(c, a) {
    return c & ~a
}
function bnAndNot(c) {
    var a = nbi();
    return this.bitwiseTo(c, op_andnot, a),
    a
}
function bnNot() {
    for (var c = nbi(), a = 0; a < this.t; ++a)
        c[a] = this.DM & ~this[a];
    return c.t = this.t,
    c.s = ~this.s,
    c
}
function bnShiftLeft(c) {
    var a = nbi();
    return 0 > c ? this.rShiftTo(-c, a) : this.lShiftTo(c, a),
    a
}
function bnShiftRight(c) {
    var a = nbi();
    return 0 > c ? this.lShiftTo(-c, a) : this.rShiftTo(c, a),
    a
}
function lbit(c) {
    if (0 == c)
        return -1;
    var d = 0;
    return 0 == (65535 & c) && (c >>= 16,
    d += 16),
    0 == (255 & c) && (c >>= 8,
    d += 8),
    0 == (15 & c) && (c >>= 4,
    d += 4),
    0 == (3 & c) && (c >>= 2,
    d += 2),
    0 == (1 & c) && ++d,
    d
}
function bnGetLowestSetBit() {
    for (var b = 0; b < this.t; ++b)
        if (0 != this[b])
            return b * this.DB + lbit(this[b]);
    return 0 > this.s ? this.t * this.DB : -1
}
function cbit(c) {
    for (var d = 0; 0 != c; )
        c &= c - 1,
        ++d;
    return d
}
function bnBitCount() {
    for (var d = 0, e = this.s & this.DM, b = 0; b < this.t; ++b)
        d += cbit(this[b] ^ e);
    return d
}
function bnTestBit(c) {
    var a = Math.floor(c / this.DB);
    return a >= this.t ? 0 != this.s : 0 != (this[a] & 1 << c % this.DB)
}
function bnpChangeBit(d, a) {
    var b = BigInteger.ONE.shiftLeft(d);
    return this.bitwiseTo(b, a, b),
    b
}
function bnSetBit(b) {
    return this.changeBit(b, op_or)
}
function bnClearBit(b) {
    return this.changeBit(b, op_andnot)
}
function bnFlipBit(b) {
    return this.changeBit(b, op_xor)
}
function bnpAddTo(f, a) {
    for (var b = 0, g = 0, h = Math.min(f.t, this.t); b < h; )
        g += this[b] + f[b],
        a[b++] = g & this.DM,
        g >>= this.DB;
    if (f.t < this.t) {
        for (g += f.s; b < this.t; )
            g += this[b],
            a[b++] = g & this.DM,
            g >>= this.DB;
        g += this.s
    } else {
        for (g += this.s; b < f.t; )
            g += f[b],
            a[b++] = g & this.DM,
            g >>= this.DB;
        g += f.s
    }
    a.s = 0 > g ? -1 : 0,
    0 < g ? a[b++] = g : -1 > g && (a[b++] = this.DV + g),
    a.t = b,
    a.clamp()
}
function bnAdd(c) {
    var a = nbi();
    return this.addTo(c, a),
    a
}
function bnSubtract(c) {
    var a = nbi();
    return this.subTo(c, a),
    a
}
function bnMultiply(c) {
    var a = nbi();
    return this.multiplyTo(c, a),
    a
}
function bnDivide(c) {
    var a = nbi();
    return this.divRemTo(c, a, null),
    a
}
function bnRemainder(c) {
    var a = nbi();
    return this.divRemTo(c, null, a),
    a
}
function bnDivideAndRemainder(d) {
    var a = nbi()
      , b = nbi();
    return this.divRemTo(d, a, b),
    [a, b]
}
function bnpDMultiply(b) {
    this[this.t] = this.am(0, b - 1, this, 0, 0, this.t),
    ++this.t,
    this.clamp()
}
function bnpDAddOffset(c, a) {
    for (; this.t <= a; )
        this[this.t++] = 0;
    for (this[a] += c; this[a] >= this.DV; )
        this[a] -= this.DV,
        ++a >= this.t && (this[this.t++] = 0),
        ++this[a]
}
function NullExp() {}
function nNop(b) {
    return b
}
function nMulTo(d, a, b) {
    d.multiplyTo(a, b)
}
function nSqrTo(c, a) {
    c.squareTo(a)
}
NullExp.prototype.convert = nNop,
NullExp.prototype.revert = nNop,
NullExp.prototype.mulTo = nMulTo,
NullExp.prototype.sqrTo = nSqrTo;
function bnPow(b) {
    return this.exp(b, new NullExp)
}
function bnpMultiplyLowerTo(f, a, b) {
    var c = Math.min(this.t + f.t, a);
    for (b.s = 0,
    b.t = c; 0 < c; )
        b[--c] = 0;
    var g;
    for (g = b.t - this.t; c < g; ++c)
        b[c + this.t] = this.am(0, f[c], b, c, 0, this.t);
    for (g = Math.min(f.t, a); c < g; ++c)
        this.am(0, f[c], b, c, 0, a - c);
    b.clamp()
}
function bnpMultiplyUpperTo(e, a, f) {
    --a;
    var c = f.t = this.t + e.t - a;
    for (f.s = 0; 0 <= --c; )
        f[c] = 0;
    for (c = Math.max(a - this.t, 0); c < e.t; ++c)
        f[this.t + c - a] = this.am(a - c, e[c], f, 0, 0, this.t + c - a);
    f.clamp(),
    f.drShiftTo(1, f)
}
function Barrett(b) {
    this.r2 = nbi(),
    this.q3 = nbi(),
    BigInteger.ONE.dlShiftTo(2 * b.t, this.r2),
    this.mu = this.r2.divide(b),
    this.m = b
}
function barrettConvert(c) {
    if (0 > c.s || c.t > 2 * this.m.t)
        return c.mod(this.m);
    if (0 > c.compareTo(this.m))
        return c;
    var a = nbi();
    return c.copyTo(a),
    this.reduce(a),
    a
}
function barrettRevert(b) {
    return b
}
function barrettReduce(b) {
    for (b.drShiftTo(this.m.t - 1, this.r2),
    b.t > this.m.t + 1 && (b.t = this.m.t + 1,
    b.clamp()),
    this.mu.multiplyUpperTo(this.r2, this.m.t + 1, this.q3),
    this.m.multiplyLowerTo(this.q3, this.m.t + 1, this.r2); 0 > b.compareTo(this.r2); )
        b.dAddOffset(1, this.m.t + 1);
    for (b.subTo(this.r2, b); 0 <= b.compareTo(this.m); )
        b.subTo(this.m, b)
}
function barrettSqrTo(c, a) {
    c.squareTo(a),
    this.reduce(a)
}
function barrettMulTo(d, a, b) {
    d.multiplyTo(a, b),
    this.reduce(b)
}
Barrett.prototype.convert = barrettConvert,
Barrett.prototype.revert = barrettRevert,
Barrett.prototype.reduce = barrettReduce,
Barrett.prototype.mulTo = barrettMulTo,
Barrett.prototype.sqrTo = barrettSqrTo;
function bnModPow(o, a) {
    var b, p, q = o.bitLength(), r = nbv(1);
    if (0 >= q)
        return r;
    b = 18 > q ? 1 : 48 > q ? 3 : 144 > q ? 4 : 768 > q ? 5 : 6,
    p = 8 > q ? new Classic(a) : a.isEven() ? new Barrett(a) : new Montgomery(a);
    var s = []
      , h = 3
      , t = b - 1
      , k = (1 << b) - 1;
    if (s[1] = p.convert(this),
    1 < b)
        for (q = nbi(),
        p.sqrTo(s[1], q); h <= k; )
            s[h] = nbi(),
            p.mulTo(q, s[h - 2], s[h]),
            h += 2;
    var m, u = o.t - 1, v = !0, w = nbi();
    for (q = nbits(o[u]) - 1; 0 <= u; ) {
        for (q >= t ? m = o[u] >> q - t & k : (m = (o[u] & (1 << q + 1) - 1) << t - q,
        0 < u && (m |= o[u - 1] >> this.DB + q - t)),
        h = b; 0 == (1 & m); )
            m >>= 1,
            --h;
        if (0 > (q -= h) && (q += this.DB,
        --u),
        v)
            s[m].copyTo(r),
            v = !1;
        else {
            for (; 1 < h; )
                p.sqrTo(r, w),
                p.sqrTo(w, r),
                h -= 2;
            0 < h ? p.sqrTo(r, w) : (h = r,
            r = w,
            w = h),
            p.mulTo(w, s[m], r)
        }
        for (; 0 <= u && 0 == (o[u] & 1 << q); )
            p.sqrTo(r, w),
            h = r,
            r = w,
            w = h,
            0 > --q && (q = this.DB - 1,
            --u)
    }
    return p.revert(r)
}
function bnGCD(e) {
    var f = 0 > this.s ? this.negate() : this.clone();
    if (e = 0 > e.s ? e.negate() : e.clone(),
    0 > f.compareTo(e)) {
        var g = f;
        f = e,
        e = g
    }
    g = f.getLowestSetBit();
    var h = e.getLowestSetBit();
    if (0 > h)
        return f;
    for (g < h && (h = g),
    0 < h && (f.rShiftTo(h, f),
    e.rShiftTo(h, e)); 0 < f.signum(); )
        0 < (g = f.getLowestSetBit()) && f.rShiftTo(g, f),
        0 < (g = e.getLowestSetBit()) && e.rShiftTo(g, e),
        0 <= f.compareTo(e) ? (f.subTo(e, f),
        f.rShiftTo(1, f)) : (e.subTo(f, e),
        e.rShiftTo(1, e));
    return 0 < h && e.lShiftTo(h, e),
    e
}
function bnpModInt(e) {
    if (0 >= e)
        return 0;
    var a = this.DV % e
      , b = 0 > this.s ? e - 1 : 0;
    if (0 < this.t)
        if (0 == a)
            b = this[0] % e;
        else
            for (var f = this.t - 1; 0 <= f; --f)
                b = (a * b + this[f]) % e;
    return b
}
function bnModInverse(i) {
    var a = i.isEven();
    if (this.isEven() && a || 0 == i.signum())
        return BigInteger.ZERO;
    for (var b = i.clone(), c = this.clone(), d = nbv(1), e = nbv(0), g = nbv(0), h = nbv(1); 0 != b.signum(); ) {
        for (; b.isEven(); )
            b.rShiftTo(1, b),
            a ? ((!d.isEven() || !e.isEven()) && (d.addTo(this, d),
            e.subTo(i, e)),
            d.rShiftTo(1, d)) : e.isEven() || e.subTo(i, e),
            e.rShiftTo(1, e);
        for (; c.isEven(); )
            c.rShiftTo(1, c),
            a ? ((!g.isEven() || !h.isEven()) && (g.addTo(this, g),
            h.subTo(i, h)),
            g.rShiftTo(1, g)) : h.isEven() || h.subTo(i, h),
            h.rShiftTo(1, h);
        0 <= b.compareTo(c) ? (b.subTo(c, b),
        a && d.subTo(g, d),
        e.subTo(h, e)) : (c.subTo(b, c),
        a && g.subTo(d, g),
        h.subTo(e, h))
    }
    if (0 != c.compareTo(BigInteger.ONE))
        return BigInteger.ZERO;
    if (0 <= h.compareTo(i))
        return h.subtract(i);
    if (0 > h.signum())
        h.addTo(i, h);
    else
        return h;
    return 0 > h.signum() ? h.add(i) : h
}
var lowprimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509]
  , lplim = 67108864 / lowprimes[lowprimes.length - 1];
function bnIsProbablePrime(f) {
    var a, g = this.abs();
    if (1 == g.t && g[0] <= lowprimes[lowprimes.length - 1]) {
        for (a = 0; a < lowprimes.length; ++a)
            if (g[0] == lowprimes[a])
                return !0;
        return !1
    }
    if (g.isEven())
        return !1;
    for (a = 1; a < lowprimes.length; ) {
        for (var c = lowprimes[a], h = a + 1; h < lowprimes.length && c < lplim; )
            c *= lowprimes[h++];
        for (c = g.modInt(c); a < h; )
            if (0 == c % lowprimes[a++])
                return !1
    }
    return g.millerRabin(f)
}
function bnpMillerRabin(i) {
    var j = this.subtract(BigInteger.ONE)
      , b = j.getLowestSetBit();
    if (0 >= b)
        return !1;
    var c = j.shiftRight(b);
    i = i + 1 >> 1,
    i > lowprimes.length && (i = lowprimes.length);
    for (var d = nbi(), e = 0; e < i; ++e) {
        d.fromInt(lowprimes[e]);
        var k = d.modPow(c, this);
        if (0 != k.compareTo(BigInteger.ONE) && 0 != k.compareTo(j)) {
            for (var l = 1; l++ < b && 0 != k.compareTo(j); )
                if (k = k.modPowInt(2, this),
                0 == k.compareTo(BigInteger.ONE))
                    return !1;
            if (0 != k.compareTo(j))
                return !1
        }
    }
    return !0
}
BigInteger.prototype.chunkSize = bnpChunkSize,
BigInteger.prototype.toRadix = bnpToRadix,
BigInteger.prototype.fromRadix = bnpFromRadix,
BigInteger.prototype.fromNumber = bnpFromNumber,
BigInteger.prototype.bitwiseTo = bnpBitwiseTo,
BigInteger.prototype.changeBit = bnpChangeBit,
BigInteger.prototype.addTo = bnpAddTo,
BigInteger.prototype.dMultiply = bnpDMultiply,
BigInteger.prototype.dAddOffset = bnpDAddOffset,
BigInteger.prototype.multiplyLowerTo = bnpMultiplyLowerTo,
BigInteger.prototype.multiplyUpperTo = bnpMultiplyUpperTo,
BigInteger.prototype.modInt = bnpModInt,
BigInteger.prototype.millerRabin = bnpMillerRabin,
BigInteger.prototype.clone = bnClone,
BigInteger.prototype.intValue = bnIntValue,
BigInteger.prototype.byteValue = bnByteValue,
BigInteger.prototype.shortValue = bnShortValue,
BigInteger.prototype.signum = bnSigNum,
BigInteger.prototype.toByteArray = bnToByteArray,
BigInteger.prototype.equals = bnEquals,
BigInteger.prototype.min = bnMin,
BigInteger.prototype.max = bnMax,
BigInteger.prototype.and = bnAnd,
BigInteger.prototype.or = bnOr,
BigInteger.prototype.xor = bnXor,
BigInteger.prototype.andNot = bnAndNot,
BigInteger.prototype.not = bnNot,
BigInteger.prototype.shiftLeft = bnShiftLeft,
BigInteger.prototype.shiftRight = bnShiftRight,
BigInteger.prototype.getLowestSetBit = bnGetLowestSetBit,
BigInteger.prototype.bitCount = bnBitCount,
BigInteger.prototype.testBit = bnTestBit,
BigInteger.prototype.setBit = bnSetBit,
BigInteger.prototype.clearBit = bnClearBit,
BigInteger.prototype.flipBit = bnFlipBit,
BigInteger.prototype.add = bnAdd,
BigInteger.prototype.subtract = bnSubtract,
BigInteger.prototype.multiply = bnMultiply,
BigInteger.prototype.divide = bnDivide,
BigInteger.prototype.remainder = bnRemainder,
BigInteger.prototype.divideAndRemainder = bnDivideAndRemainder,
BigInteger.prototype.modPow = bnModPow,
BigInteger.prototype.modInverse = bnModInverse,
BigInteger.prototype.pow = bnPow,
BigInteger.prototype.gcd = bnGCD,
BigInteger.prototype.isProbablePrime = bnIsProbablePrime;
var rng_state, rng_pool, rng_pptr;
function rng_seed_int(b) {
    rng_pool[rng_pptr++] ^= 255 & b,
    rng_pool[rng_pptr++] ^= 255 & b >> 8,
    rng_pool[rng_pptr++] ^= 255 & b >> 16,
    rng_pool[rng_pptr++] ^= 255 & b >> 24,
    rng_pptr >= rng_psize && (rng_pptr -= rng_psize)
}
function rng_seed_time() {
    rng_seed_int(new Date().getTime())
}
if (null == rng_pool) {
    rng_pool = [],
    rng_pptr = 0;
    var t;
    if ("Netscape" == navigator.appName && "5" > navigator.appVersion && window.crypto) {
        var z = window.crypto.random(32);
        for (t = 0; t < z.length; ++t)
            rng_pool[rng_pptr++] = 255 & z.charCodeAt(t)
    }
    for (; rng_pptr < rng_psize; )
        t = Math.floor(65536 * Math.random()),
        rng_pool[rng_pptr++] = t >>> 8,
        rng_pool[rng_pptr++] = 255 & t;
    rng_pptr = 0,
    rng_seed_time()
}
function rng_get_byte() {
    if (null == rng_state) {
        for (rng_seed_time(),
        rng_state = prng_newstate(),
        rng_state.init(rng_pool),
        rng_pptr = 0; rng_pptr < rng_pool.length; ++rng_pptr)
            rng_pool[rng_pptr] = 0;
        rng_pptr = 0
    }
    return rng_state.next()
}
function rng_get_bytes(c) {
    var a;
    for (a = 0; a < c.length; ++a)
        c[a] = rng_get_byte()
}
function SecureRandom() {}
SecureRandom.prototype.nextBytes = rng_get_bytes;
function Arcfour() {
    this.j = this.i = 0,
    this.S = []
}
function ARC4init(e) {
    var b, f, g;
    for (b = 0; 256 > b; ++b)
        this.S[b] = b;
    for (b = f = 0; 256 > b; ++b)
        f = 255 & f + this.S[b] + e[b % e.length],
        g = this.S[b],
        this.S[b] = this.S[f],
        this.S[f] = g;
    this.j = this.i = 0
}
function ARC4next() {
    var a;
    return this.i = 255 & this.i + 1,
    this.j = 255 & this.j + this.S[this.i],
    a = this.S[this.i],
    this.S[this.i] = this.S[this.j],
    this.S[this.j] = a,
    this.S[255 & a + this.S[this.i]]
}
Arcfour.prototype.init = ARC4init,
Arcfour.prototype.next = ARC4next;
function prng_newstate() {
    return new Arcfour
}
var rng_psize = 256;
function parseBigInt(c, a) {
    return new BigInteger(c,a)
}
function linebrk(e, a) {
    for (var b = "", f = 0; f + a < e.length; )
        b += e.substring(f, f + a) + "\n",
        f += a;
    return b + e.substring(f, e.length)
}
function byte2Hex(b) {
    return 16 > b ? "0" + b.toString(16) : b.toString(16)
}
function pkcs1pad2(f, a) {
    if (a < f.length + 11)
        return alert("Message too long for RSA - Max length is " + (a - 11) + " bytes"),
        null;
    for (var g = [], d = f.length - 1; 0 <= d && 0 < a; )
        g[--a] = f.charCodeAt(d--);
    g[--a] = 0,
    d = new SecureRandom;
    for (var h = []; 2 < a; ) {
        for (h[0] = 0; 0 == h[0]; )
            d.nextBytes(h);
        g[--a] = h[0]
    }
    return g[--a] = 2,
    g[--a] = 0,
    new BigInteger(g)
}
function RSAKey() {
    this.n = null,
    this.e = 0,
    this.coeff = this.dmq1 = this.dmp1 = this.q = this.p = this.d = null
}
function RSASetPublic(c, a) {
    null != c && null != a && 0 < c.length && 0 < a.length ? (this.n = parseBigInt(c, 16),
    this.e = parseInt(a, 16)) : alert("Invalid RSA public key")
}
function RSADoPublic(b) {
    return b.modPowInt(this.e, this.n)
}
function RSAEncrypt(b) {
    return (b = pkcs1pad2(b, this.n.bitLength() + 7 >> 3),
    null == b) ? null : (b = this.doPublic(b),
    null == b) ? null : (b = b.toString(16),
    0 == (1 & b.length) ? b : "0" + b)
}
RSAKey.prototype.doPublic = RSADoPublic,
RSAKey.prototype.setPublic = RSASetPublic,
RSAKey.prototype.encrypt = RSAEncrypt;
function pkcs1unpad2(f, a) {
    for (var b = f.toByteArray(), c = 0; c < b.length && 0 == b[c]; )
        ++c;
    if (b.length - c != a - 1 || 2 != b[c])
        return null;
    for (++c; 0 != b[c]; )
        if (++c >= b.length)
            return null;
    for (var g = ""; ++c < b.length; )
        g += String.fromCharCode(b[c]);
    return g
}
function RSASetPrivate(d, a, b) {
    null != d && null != a && 0 < d.length && 0 < a.length ? (this.n = parseBigInt(d, 16),
    this.e = parseInt(a, 16),
    this.d = parseBigInt(b, 16)) : alert("Invalid RSA private key")
}
function RSASetPrivateEx(i, a, b, c, d, e, f, g) {
    null != i && null != a && 0 < i.length && 0 < a.length ? (this.n = parseBigInt(i, 16),
    this.e = parseInt(a, 16),
    this.d = parseBigInt(b, 16),
    this.p = parseBigInt(c, 16),
    this.q = parseBigInt(d, 16),
    this.dmp1 = parseBigInt(e, 16),
    this.dmq1 = parseBigInt(f, 16),
    this.coeff = parseBigInt(g, 16)) : alert("Invalid RSA private key")
}
function RSAGenerate(i, a) {
    var b = new SecureRandom
      , c = i >> 1;
    this.e = parseInt(a, 16);
    for (var d = new BigInteger(a,16); ; ) {
        for (; ; )
            if (this.p = new BigInteger(i - c,1,b),
            0 == this.p.subtract(BigInteger.ONE).gcd(d).compareTo(BigInteger.ONE) && this.p.isProbablePrime(10))
                break;
        for (; ; )
            if (this.q = new BigInteger(c,1,b),
            0 == this.q.subtract(BigInteger.ONE).gcd(d).compareTo(BigInteger.ONE) && this.q.isProbablePrime(10))
                break;
        if (0 >= this.p.compareTo(this.q)) {
            var e = this.p;
            this.p = this.q,
            this.q = e
        }
        e = this.p.subtract(BigInteger.ONE);
        var j = this.q.subtract(BigInteger.ONE)
          , g = e.multiply(j);
        if (0 == g.gcd(d).compareTo(BigInteger.ONE)) {
            this.n = this.p.multiply(this.q),
            this.d = d.modInverse(g),
            this.dmp1 = this.d.mod(e),
            this.dmq1 = this.d.mod(j),
            this.coeff = this.q.modInverse(this.p);
            break
        }
    }
}
function RSADoPrivate(c) {
    if (null == this.p || null == this.q)
        return c.modPow(this.d, this.n);
    var d = c.mod(this.p).modPow(this.dmp1, this.p);
    for (c = c.mod(this.q).modPow(this.dmq1, this.q); 0 > d.compareTo(c); )
        d = d.add(this.p);
    return d.subtract(c).multiply(this.coeff).mod(this.p).multiply(this.q).add(c)
}
function RSADecrypt(b) {
    return b = this.doPrivate(parseBigInt(b, 16)),
    null == b ? null : pkcs1unpad2(b, this.n.bitLength() + 7 >> 3)
}
RSAKey.prototype.doPrivate = RSADoPrivate,
RSAKey.prototype.setPrivate = RSASetPrivate,
RSAKey.prototype.setPrivateEx = RSASetPrivateEx,
RSAKey.prototype.generate = RSAGenerate,
RSAKey.prototype.decrypt = RSADecrypt;
function Stream(c, a) {
    c instanceof Stream ? (this.enc = c.enc,
    this.pos = c.pos) : (this.enc = c,
    this.pos = a)
}
Stream.prototype.get = function(b) {
    if (null == b && (b = this.pos++),
    b >= this.enc.length)
        throw "Requesting byte offset " + b + " on a stream of length " + this.enc.length;
    return this.enc[b]
}
,
Stream.prototype.hexDigits = "0123456789abcdef",
Stream.prototype.hexByte = function(b) {
    return this.hexDigits.charAt(15 & b >> 4) + this.hexDigits.charAt(15 & b)
}
,
Stream.prototype.hexDump = function(e, a) {
    for (var b = "", f = e; f < a; ++f)
        b += this.hexByte(this.get(f));
    return b
}
,
Stream.prototype.parseInteger = function(c, a) {
    return this.hexDump(c, a)
}
;
function ASN1(e, a, b, d, c) {
    this.stream = e,
    this.header = a,
    this.length = b,
    this.tag = d,
    this.sub = c
}
ASN1.prototype.content = function() {
    if (this.tag == null)
        return null;
    if (0 != this.tag >> 6)
        return null == this.sub ? null : "(" + this.sub.length + ")";
    var c = 31 & this.tag
      , a = this.posContent()
      , b = Math.abs(this.length);
    return 2 == c ? this.stream.parseInteger(a, a + b) : null
}
,
ASN1.prototype.posStart = function() {
    return this.stream.pos
}
,
ASN1.prototype.posContent = function() {
    return this.stream.pos + this.header
}
,
ASN1.prototype.posEnd = function() {
    return this.stream.pos + this.header + Math.abs(this.length)
}
,
ASN1.decodeLength = function(e) {
    var a = e.get()
      , f = 127 & a;
    if (f == a)
        return f;
    if (3 < f)
        throw "Length over 24 bits not supported at position " + (e.pos - 1);
    if (0 == f)
        return -1;
    for (var d = a = 0; d < f; ++d)
        a = a << 8 | e.get();
    return a
}
,
ASN1.hasContent = function(e, a, b) {
    if (32 & e)
        return !0;
    if (3 > e || 4 < e)
        return !1;
    var d = new Stream(b);
    if (3 == e && d.get(),
    1 & d.get() >> 6)
        return !1;
    try {
        var c = ASN1.decodeLength(d);
        return d.pos - b.pos + c == a
    } catch (a) {
        return !1
    }
}
,
ASN1.decode = function(i) {
    i instanceof Stream || (i = new Stream(i,0));
    var j = new Stream(i)
      , b = i.get()
      , d = ASN1.decodeLength(i)
      , k = i.pos - j.pos
      , f = null;
    if (ASN1.hasContent(b, d, i)) {
        var l = i.pos;
        if (3 == b && i.get(),
        f = [],
        0 <= d) {
            for (var h = l + d; i.pos < h; )
                f[f.length] = ASN1.decode(i);
            if (i.pos != h)
                throw "Content size is not correct for container starting at offset " + l
        } else
            try {
                for (; ; ) {
                    if (h = ASN1.decode(i),
                    0 == h.tag)
                        break;
                    f[f.length] = h
                }
                d = l - i.pos
            } catch (a) {
                throw "Exception while decoding undefined length content: " + a
            }
    } else
        i.pos += d;
    return new ASN1(j,k,d,b,f)
}
;
var Base64 = {};
Base64.decode = function(g) {
    if (Base64.decoder == null) {
        for (var d = [], h = 0; 64 > h; ++h)
            d["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(h)] = h;
        for (h = 0; 9 > h; ++h)
            d["= \f\n\r\t\xA0\u2028\u2029".charAt(h)] = -1;
        Base64.decoder = d
    }
    d = [];
    var i = 0
      , j = 0;
    for (h = 0; h < g.length; ++h) {
        var k = g.charAt(h);
        if ("=" == k)
            break;
        if (k = Base64.decoder[k],
        -1 != k) {
            if (null == k)
                throw "Illegal character at offset " + h;
            i |= k,
            4 <= ++j ? (d[d.length] = i >> 16,
            d[d.length] = 255 & i >> 8,
            d[d.length] = 255 & i,
            j = i = 0) : i <<= 6
        }
    }
    switch (j) {
    case 1:
        throw "Base64 encoding incomplete: at least 2 bits missing";
    case 2:
        d[d.length] = i >> 10;
        break;
    case 3:
        d[d.length] = i >> 16,
        d[d.length] = 255 & i >> 8;
    }
    return d
}
,
Base64.re = /-----BEGIN [^-]+-----([A-Za-z0-9+\/=\s]+)-----END [^-]+-----|begin-base64[^\n]+\n([A-Za-z0-9+\/=\s]+)====/,
Base64.unarmor = function(b) {
    var c = Base64.re.exec(b);
    return c && (c[1] ? b = c[1] : c[2] && (b = c[2])),
    Base64.decode(b)
}
;
var ASNIntValue, ASNLength, int2hex;
RSAKey.prototype.privatePEM = function() {
    var a;
    return a = "020100",
    a += ASNIntValue(this.n, !0),
    a += ASNIntValue(this.e, !1),
    a += ASNIntValue(this.d, !1),
    a += ASNIntValue(this.p, !0),
    a += ASNIntValue(this.q, !0),
    a += ASNIntValue(this.dmp1, !0),
    a += ASNIntValue(this.dmq1, !1),
    a += ASNIntValue(this.coeff, !1),
    a = "30" + ASNLength(a) + a,
    "-----BEGIN RSA PRIVATE KEY-----\n" + encode64(chars_from_hex(a)) + "\n-----END RSA PRIVATE KEY-----"
}
,
RSAKey.prototype.publicPEM = function() {
    var a;
    return a = ASNIntValue(this.n, !0),
    a += ASNIntValue(this.e, !1),
    a = "30" + ASNLength(a) + a,
    "-----BEGIN RSA PUBLIC KEY-----\n" + encode64(chars_from_hex(a)) + "\n-----END RSA PUBLIC KEY-----"
}
,
RSAKey.prototype.parsePrivatePEM = function(a) {
    var a = ASN1.decode(Base64.unarmor(a)).sub;
    return this.setPrivateEx(a[1].content(), a[2].content(), a[3].content(), a[4].content(), a[5].content(), a[6].content(), a[7].content(), a[8].content())
}
,
RSAKey.prototype.parsePublicPEM = function(a) {
    var a = ASN1.decode(Base64.unarmor(a)).sub;
    return this.setPublic(a[0].content(), a[1].content())
}
,
ASNIntValue = function(a, b) {
    return a = int2hex(a),
    b && (a = "00" + a),
    "02" + ASNLength(a) + a
}
,
ASNLength = function(a, b) {
    var c;
    return "undefined" != typeof b && null !== b || (b = 0),
    c = a.length / 2 + b,
    127 < c ? (c = int2hex(c),
    int2hex(128 + c.length / 2) + c) : int2hex(c)
}
,
int2hex = function(a) {
    return a = a.toString(16),
    0 != a.length % 2 && (a = "0" + a),
    a
}
;
export default RSAKey;

